package com.project.pedro.recipeapp.service;

import com.project.pedro.recipeapp.doamin.Recipe;
import com.project.pedro.recipeapp.repositories.RecipeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RecipeServiceImplTest {

    @Mock
    RecipeRepository recipeRepository;

    @InjectMocks
    RecipeServiceImpl recipeService;

    @Test
    void getRecipesTest(){
        //Given
        Recipe recipe = new Recipe();
        recipe.setId(1L);
        Set<Recipe> recipes = Set.of(recipe);
        when(recipeRepository.findAll()).thenReturn(recipes);

        // When
        Set<Recipe> result = recipeService.getRecipes();

        // Then
        assertEquals(result.size(), recipes.size());
        assertEquals(recipes, result);
        verify(recipeRepository, times(1)).findAll();
    }

}