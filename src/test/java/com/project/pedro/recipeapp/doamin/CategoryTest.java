package com.project.pedro.recipeapp.doamin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CategoryTest {

    Category category;

    @BeforeEach
    void setUp(){
        category = new Category();
        category.setID(1L);
        category.setDescription("description");
        Recipe recipe = new Recipe();
        category.setRecipes(Set.of(recipe));
    }

    @Test
    void getIdTest(){
        Long id = category.getID();
        assertEquals(1, id);
    }

    @Test
    void getDescriptionTest(){

    }

    @Test
    void getRecipes(){

    }

}