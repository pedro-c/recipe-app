package com.project.pedro.recipeapp.repositories;

import com.project.pedro.recipeapp.doamin.UnitOfMeasure;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@DataJpaTest
class UnitOfMeasureRepositoryTest {

    @Autowired
    UnitOfMeasureRepository unitOfMeasureRepository;

    @BeforeEach
    void setUp(){
        UnitOfMeasure unitOfMeasure = new UnitOfMeasure();
        unitOfMeasure.setDescription("test");
        unitOfMeasureRepository.save(unitOfMeasure);
    }

    @Test
    void findByDescriptionTest(){
        Optional<UnitOfMeasure> result = unitOfMeasureRepository.findByDescription("test");
        assertTrue(result.isPresent());
        assertEquals("test", result.get().getDescription());
    }


}