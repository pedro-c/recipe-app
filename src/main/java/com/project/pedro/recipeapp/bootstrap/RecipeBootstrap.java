package com.project.pedro.recipeapp.bootstrap;

import com.project.pedro.recipeapp.doamin.*;
import com.project.pedro.recipeapp.repositories.CategoryRepository;
import com.project.pedro.recipeapp.repositories.RecipeRepository;
import com.project.pedro.recipeapp.repositories.UnitOfMeasureRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@Component
@Slf4j
public class RecipeBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final CategoryRepository categoryRepository;
    private final UnitOfMeasureRepository unitOfMeasureRepository;
    private final RecipeRepository recipeRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        recipeRepository.saveAll(getRecipes());
    }

    private List<Recipe> getRecipes() {
        log.info("Creating recipes");
        List<Recipe> recipes = new ArrayList<>(2);

        log.info("Loading Units of measure");

        Optional<UnitOfMeasure> eachUomOptional = unitOfMeasureRepository.findByDescription("Each");
        if (!eachUomOptional.isPresent()){
            throw new RuntimeException("Expected Uom Not found");
        }

        Optional<UnitOfMeasure> tablespoonUomOptional = unitOfMeasureRepository.findByDescription("Tablespoon");
        if (!tablespoonUomOptional.isPresent()){
            throw new RuntimeException("Expected Uom Not found");
        }

        Optional<UnitOfMeasure> teaspoonUomOptional = unitOfMeasureRepository.findByDescription("Teaspoon");
        if (!teaspoonUomOptional.isPresent()){
            throw new RuntimeException("Expected Uom Not found");
        }

        Optional<UnitOfMeasure> dashUomOptional = unitOfMeasureRepository.findByDescription("Dash");
        if (!dashUomOptional.isPresent()){
            throw new RuntimeException("Expected Uom Not found");
        }

        Optional<UnitOfMeasure> pintUomOptional = unitOfMeasureRepository.findByDescription("Pint");
        if (!pintUomOptional.isPresent()){
            throw new RuntimeException("Expected Uom Not found");
        }

        Optional<UnitOfMeasure> cupsUomOptional = unitOfMeasureRepository.findByDescription("Cups");
        if (!cupsUomOptional.isPresent()){
            throw new RuntimeException("Expected Uom Not found");
        }

        UnitOfMeasure eachUom = eachUomOptional.get();
        UnitOfMeasure tablespoonUom = tablespoonUomOptional.get();
        UnitOfMeasure teaspoonUom = teaspoonUomOptional.get();
        UnitOfMeasure dashUom = dashUomOptional.get();
        UnitOfMeasure pintUom = pintUomOptional.get();
        UnitOfMeasure cupsUom = cupsUomOptional.get();

        log.info("Loading Categories");

        Optional<Category> mexicanOptional = categoryRepository.findByDescription("Mexican");
        if (!mexicanOptional.isPresent()){
            throw new RuntimeException("Category Not Found");
        }

        Optional<Category> americanOptional = categoryRepository.findByDescription("American");
        if (!americanOptional.isPresent()){
            throw new RuntimeException("Category Not Found");
        }

        Category mexicanCategory = mexicanOptional.get();
        Category americanCategory = americanOptional.get();

        log.info("Perfect Guacamole");
        Recipe perfectGuacamole = new Recipe();
        perfectGuacamole.setDescription("perfect Guacamole");
        perfectGuacamole.setPrepTime(10);
        perfectGuacamole.setCookTime(0);
        perfectGuacamole.setDifficulty(Difficulty.EASY);
        perfectGuacamole.setDescription("perfect Guacamole recipe description");

        Notes notes = new Notes();
        notes.setRecipeNotes("Recipe Notes");
        perfectGuacamole.addNotes(notes);


        perfectGuacamole.addIngredient(new Ingredient("ripe avocados", BigDecimal.valueOf(2), eachUom, perfectGuacamole));
        perfectGuacamole.addIngredient(new Ingredient("Kusher salt", BigDecimal.valueOf(0.5), teaspoonUom, perfectGuacamole));
        perfectGuacamole.addIngredient(new Ingredient("fresh lime or lemon juice", BigDecimal.valueOf(2), tablespoonUom, perfectGuacamole));
        perfectGuacamole.addIngredient(new Ingredient("minced red onion or thinly sliced green onion", BigDecimal.valueOf(2), tablespoonUom, perfectGuacamole));
        perfectGuacamole.addIngredient(new Ingredient("serrano chiles, stems and seeds removed, miced", BigDecimal.valueOf(2), eachUom, perfectGuacamole));
        perfectGuacamole.addIngredient(new Ingredient("Cilantro", BigDecimal.valueOf(2), tablespoonUom, perfectGuacamole));
        perfectGuacamole.addIngredient(new Ingredient("ripe tomato, seeds and pulp removed, chopped", BigDecimal.valueOf(0.5), eachUom, perfectGuacamole));

        Set<Category> categories = Set.of(americanCategory, mexicanCategory);
        perfectGuacamole.getCategories().addAll(categories);

        perfectGuacamole.setServings(2);
        perfectGuacamole.setDirections("Directions");

        recipes.add(perfectGuacamole);
        log.info("Saving recipe Perfect Guacamole");
        return recipes;
     }

}
