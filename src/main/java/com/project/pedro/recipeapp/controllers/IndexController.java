package com.project.pedro.recipeapp.controllers;

import com.project.pedro.recipeapp.doamin.Recipe;
import com.project.pedro.recipeapp.service.RecipeServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Set;

@Controller
@AllArgsConstructor
@Slf4j
public class IndexController {

    private final RecipeServiceImpl recipeService;

    @RequestMapping({"", "/", "/index", "/index.html"})
    public String index(Model model){
        Set<Recipe> recipes = recipeService.getRecipes();
        model.addAttribute("recipes", recipes);
        return"index";
    }
}
