package com.project.pedro.recipeapp.repositories;

import com.project.pedro.recipeapp.doamin.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {

    Optional<Category>  findByDescription(String description);
}
