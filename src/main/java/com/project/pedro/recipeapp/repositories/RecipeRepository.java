package com.project.pedro.recipeapp.repositories;

import com.project.pedro.recipeapp.doamin.Recipe;
import org.springframework.data.repository.CrudRepository;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {
}
