package com.project.pedro.recipeapp.service;

import com.project.pedro.recipeapp.doamin.Recipe;

import java.util.Set;

public interface RecipeService {

    Set<Recipe> getRecipes();
}
