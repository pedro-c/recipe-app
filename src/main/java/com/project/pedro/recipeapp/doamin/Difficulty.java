package com.project.pedro.recipeapp.doamin;

public enum  Difficulty {
    EASY,
    MODERATE,
    HARD
}
